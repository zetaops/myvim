myvim
=====

Nothing interesting here.
By using the way of pathogen
https://github.com/tpope/vim-pathogen
keeping my all vim files in peace.


Bundle dir
===========

ctrlp.vim         
nerdtree          
syntastic         
vim-airline       
vim-fugitive      
vim-nerdtree-tabs 
vimerl            
vimproc           
vimshell.vim
